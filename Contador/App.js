import React, { Component } from "react";
import { Platform, Dimensions } from "react-native";

import {
  createStackNavigator,
  createAppContainer,
  createDrawerNavigator
} from "react-navigation";

import Icon from "react-native-vector-icons/Ionicons";

import LoginScreen from "./src/pages/LoginScreen";
import ContagemScreen from "./src/pages/ContagemScreen";
import MenuDrawer from "./src/components/MenuDrawer";
import RelatorioScreen from "./src/pages/RelatorioScreen";
import ObservacaoScreen from './src/pages/ObservacaoScreen';
import SincronizarScreen from './src/pages/SincronizarScreen'

const WIDTH = Dimensions.get("window").width;

const DrawerConfig = {
  drawerWidth: WIDTH * 0.83,
  contentComponent: ({ navigation }) => {
    return <MenuDrawer navigation={navigation} />;
  }
};

const DrawerNavigator = createDrawerNavigator(
  {
    Contagem: {
      screen: ContagemScreen
    },
    Coordenador: {
      screen: RelatorioScreen
    },
    Observacao: {
      screen: ObservacaoScreen
    }
  },
  DrawerConfig
);

const AppNavigator = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        title: 'aaaa',
        header: null
      }
    },
    Contagem: {
      screen: DrawerNavigator,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: "Menu",
          headerLeft: (
            <Icon
              name="md-menu"
              style={{ paddingLeft: 20 }}
              size={40}
              onPress={() => navigation.openDrawer()}
            />
          ),
          headerTitleStyle: {
            fontSize: 25
          }
        };
      }
    },
    Sincronizar: {
      screen: SincronizarScreen,
      navigationOptions: {
        title: 'Voltar'
      }
    }
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
