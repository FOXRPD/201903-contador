import React, { Component } from "react";
import { View, Text, TextInput, Button, Alert, StyleSheet } from "react-native";
import SqlService from "../../provider/SqlService";
import moment from 'moment';

export default class ObservacaoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ""
    };
  }

  sendMessage() {
    // if (this.state.message == "") return;
    let d = moment(new Date()).format("DD-MM-YYYY HH:mm:ss")
// {this.props.navigation.state.params.session_id}
    Alert.alert("Enviar", "Deseja enviar essa mensagem?",[
      {
        text: "Cancelar"
      },
      {
        text: "Enviar",
        onPress: () => {
          SqlService.insert('Observacao', ['datetime', 'session_id', 'texto'], [d, this.props.navigation.state.params.session_id, this.state.message])
          // .the(() => (
            Alert.alert('Sucesso!','Mensagem enviada com Sucesso!')
            this.setState({ message: '' })
            this.props.navigation.navigate('Contagem')
            // ))
          }
        }
      ],
      { cancelable: false });

  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Text style={styles.header}>Observação</Text>
        <TextInput
        autoFocus
          multiline
          style={styles.textInput}
          placeholder="Digite a sua observação aqui!"
          value = {this.state.message}
          onChangeText={value => this.setState({ message: value })}
        />
        <View style={{ paddingLeft: 200, paddingRight: 200 }}>
          <Button
            title="Enviar"
            onPress={() => this.sendMessage()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    marginLeft: 60,
    marginRight: 60,
    marginBottom: 20,
    borderWidth: 2,
    borderColor: "gray",
    height: 400,
    fontSize: 30,
    textAlign: "center",
    color: "gray"
  },
  header: {
    fontSize: 30,
    fontWeight: "bold",
    color: "#2872c0",
    textAlign: "center",
    marginTop: 30,
    marginBottom: 10
  }
});
