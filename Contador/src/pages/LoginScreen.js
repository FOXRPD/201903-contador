/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Alert,
  Image,
  ImageBackground,
  YellowBox
} from "react-native";
import DialogInput from "react-native-dialog-input";
import FormRow from "../components/FormRow";
import SqlService from "../../provider/SqlService";
import { Dropdown } from "react-native-material-dropdown";
import moment from "moment";

export default class LoginPage extends React.Component {
  constructor(props) {
    super();
    YellowBox.ignoreWarnings([
      "Remote debugger is in a background tab which may cause apps to perform slowly."
    ]);

    this.state = {
      restart: true,
      isDialogVisible: false,
      session_id: -1,
      tablet: "",
      usuario: "",
      ids: [],
      message: "",
      pontos: [
        {
          id: -1,
          desc: ""
        }
      ],
      pontoSelecionado: {
        id: -1,
        label: ""
      }
    };
  }

  async componentDidMount() {
    let check = await SqlService.createDatabase();

    if (check) {
      SqlService.select("CONFIG").then(res => {
        res.map(t => {
          if (t["key"] == "Android_id") {
            console.log(t["value"].split("_")[0]);
            this.setState({ tablet: t["value"].split("_")[0] });
          }
        });

        SqlService.select("User", "*").then(res =>
          this.setState({ ids: res.map(item => item["id"]) })
        );
        SqlService.select("Point", "*").then(res =>
          this.setState({ pontos: res })
        );
        this.setState({
          restart: false
        });
        // }
      });
    } else {
      this.setState({
        isDialogVisible: true,
        restart: true,
        message: "Reinicie o aplicativo!"
      });
    }
  }

  onChangeInput(field, value) {
    this.setState({
      [field]: value
    });
  }

  async tryLogin() {
    const { ids, usuario, pontoSelecionado } = this.state;

    if (usuario.includes("202020")) {
      this.props.navigation.navigate("Sincronizar");
    }

    if (usuario.length != 5) {
      this.setState({ message: "Login deve conter 5 dígitos!" });
      return;
    }

    if (this.state.pontoSelecionado.id < 0) {
      this.setState({ message: "Selecione o ponto!" });
      return;
    }

    try {
      if (ids.includes(parseInt(usuario))) {
        let d = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
        SqlService.insert(
          "Session",
          ["datetime", "user", "point_id"],
          [d, usuario, pontoSelecionado.id]
        );

        await new Promise((resolve, reject) => {
          return SqlService.select(
            "Session",
            "id",
            " datetime == ''" + d + "' ",
            null,
            " id desc limit 1"
          ).then(id => {
            let session_id = id[0].id;

            if (session_id > 0) {
              this.setState({
                session_id: -1,
                message: "",
                usuario: "",
                pontoSelecionado: {
                  id: -1,
                  label: ""
                }
              });

              this.props.navigation.navigate("Contagem", {
                usuario,
                pontoSelecionado,
                session_id
              });
            } else {
              Alert.alert("Erro", "Erro ao buscar sessão criada!");
            }
          });
        });
      } else {
        this.setState({ message: "Login inválido!" });
      }
    } catch (e) {
      Alert.alert("Erro no login", "Erro no catch do tryLogin()! " + e);
    }
  }

  renderMessage() {
    const { message } = this.state;

    if (!message) return null;

    return (
      <View>
        <Text style={styles.erro}>{this.state.message}</Text>
      </View>
    );
  }

  render() {
    return (
      <ImageBackground
        source={require("../image/fundo.png")}
        style={{ width: "100%", height: "100%" }}
      >
        <View style={styles.container}>
          <Image source={require("../image/logo.png")} />
          <DialogInput
            isDialogVisible={this.state.isDialogVisible}
            title={"Atenção"}
            message={"Insira o número do Tablet"}
            hintInput={"Mínimo 3 dígitos"}
            cancelText="Confirmar"
            submitInput={inputText => {
              console.log(inputText);
              if (inputText != "" && inputText.length > 2) {
                SqlService.insert(
                  "Config",
                  ["key", "value"],
                  [
                    "Android_id",
                    `${inputText.toUpperCase()}_${moment(new Date()).format(
                      "YYYYMMDDHHmmss"
                    )}`
                  ]
                )
                  .then(() =>
                    SqlService.insert(
                      "Config",
                      ["key", "value"],
                      [
                        "date_Sinc",
                        moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
                      ]
                    )
                  )
                  .then(() =>
                    SqlService.insert(
                      "Config",
                      ["key", "value"],
                      ["last_idData_sinc", 0]
                    ).then(() =>
                      SqlService.insert(
                        "Config",
                        ["key", "value"],
                        ["obs_sinc", 0]
                      ).then(
                        () =>
                          Alert.alert(
                            "Atenção!!!",
                            "O aplicativo precisa ser Reiniciado!"
                          ),
                        this.setState({
                          tablet: inputText.toUpperCase(),
                          isDialogVisible: false
                        })
                      )
                    )
                  );
              }
            }}
            closeDialog={() => {}}
          />
          <FormRow first>
            <TextInput
              autoFocus
              style={styles.input}
              placeholder="Usuário"
              keyboardType="numeric"
              value={this.state.usuario}
              underlineColorAndroid="#CECECE"
              onChangeText={value => this.onChangeInput("usuario", value)}
            />
          </FormRow>
          <View style={{ width: 400 }}>
            <Dropdown
              animationDuration={0.1}
              style={{ fontSize: 20 }}
              itemPadding={10}
              label={
                this.state.pontoSelecionado.id < 0
                  ? "Selecione o Ponto"
                  : "Ponto"
              }
              textColor="#2872c0"
              itemCount={10}
              data={this.state.pontos}
              valueExtractor={v => v}
              labelExtractor={({ desc }) => desc}
              onChangeText={v =>
                this.setState({ pontoSelecionado: { id: v.id, label: v.desc } })
              }
            />
            <View style={styles.buttonStyle}>
              <Button
                title="Entrar"
                disabled={this.state.restart}
                onPress={() => this.tryLogin()}
              />
            </View>
          </View>
          {this.renderMessage()}
        </View>
        <Text>Versão: 1.0</Text>
        <Text>Tablet: {this.state.tablet}</Text>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 125
  },
  input: {
    paddingBottom: 5,
    fontSize: 25,
    color: "#2872c0",
    textAlign: "center",
    width: 400
  },
  buttonStyle: {
    paddingTop: 40,
    paddingLeft: 90,
    paddingRight: 90
  },
  erro: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    color: "red"
  }
});
