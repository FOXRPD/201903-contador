import React, { Component } from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  NetInfo,
  ActivityIndicator,
  Alert
} from "react-native";
import SqlService from "../../provider/SqlService";
import firebase from "firebase";
import moment from "moment";
import Icon from "react-native-vector-icons/AntDesign";

export default class SincronizarScreen extends Component {
  constructor(props) {
    super();

    this.state = {
      connection_Status: "",
      isLoading: false,
      dateSinc: "Não encontrado"
    };
  }

  _handleConnectivityChange = isConnected => {
    if (isConnected == true) {
      this.setState({ connection_Status: "Online" });
    } else {
      this.setState({ connection_Status: "Offline" });
    }
  };

  getUnique(dados, campo) {
    let unico = [];

    dados.filter(da => {
      if (unico.indexOf(da[campo]) === -1) {
        unico.push(da[campo]);
      }
    });
    return unico;
  }

  countData(dados, ponto, dia, hora, user) {
    return dados
      .filter(e => {
        if (
          e.Ponto == ponto &&
          e.dia == dia &&
          e.hora == hora &&
          e.user == user &&
          e.contagem > 0
        ) {
          return e;
        }
      })
      .map(m => {
        return {
          estacao: m.entrada,
          quantidade: m.contagem
        };
      });
  }

  //select desc as Ponto, strftime('%Y-%m-%d %H', d.dateregistered) as dt, entrada, count(d.id) as contagem from data d join session s on d.session_id = s.id join point p on s.point_id = p.id where s.user != 99999 group by Ponto, entrada, strftime('%Y-%m-%d %H', d.dateregistered) order by ponto, dt
  async sinc() {
    console.log("Iniciando o sinc");
    this.setState({ isLoading: true });
    let day = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

    const dataSinc = await SqlService.query(
      "select desc as Ponto, strftime('%d-%m-%Y', d.dateregistered) as dia, strftime('%H:00', d.dateregistered) as hora, entrada, count(d.id) as contagem, s.user from data d join session s on d.session_id = s.id join point p on s.point_id = p.id where d.id > (select value from config where key = 'last_idData_sinc') and s.user != 99999 group by Ponto, entrada, strftime('%Y-%m-%d %H', d.dateregistered), s.user order by ponto, dia, hora"
    );

    console.log('dataObservacao')
    const dataObservacao = await SqlService.query(
      "select s.user, p.desc as ponto, o.id, o.datetime, o.texto from Observacao o join Session s on s.id = o.session_id join point p on s.point_id = p.id where o.id > (SELECT value FROM CONFIG WHERE KEY = 'obs_sinc')"
    ).then(a => {
      console.log('res')
      console.log(a)
      let obs = []
      a.map(o => {
        
      console.log(o.texto)
        obs.push({
          ponto: o.ponto,
          usuario: o.user,
          data: o.datetime,
          texto: o.texto
        });
      });
      return obs
    });

    console.log('dataObservacao');
    console.log(dataObservacao);
    if (dataSinc.length == 0 && dataObservacao.length == 0) {
      Alert.alert("Atenção", "Não a dados para serem enviados!");
      this.setState({ isLoading: false });
      return;
    }

    const uPontos = await this.getUnique(dataSinc, "Ponto");
    const uDias = await this.getUnique(dataSinc, "dia");
    const uHoras = await this.getUnique(dataSinc, "hora");
    const uUser = await this.getUnique(dataSinc, "user");

    let dataObject = {
      dataSinc: day,
      Observacao: dataObservacao,
      pontos: await uPontos.map(up => ({
        ponto: up,
        dias: uDias.map(ud => ({
          dia: ud,
          horas: uHoras.map(uh => ({
            hora: uh,
            usuarios: uUser.map(us => ({
              usuario: us,
              contagem: this.countData(dataSinc, up, ud, uh, us)
            }))
          }))
        }))
      }))
    };

    await SqlService.select(
      "Config",
      "value",
      "key = 'Android_id'",
      "'Android_id'"
    ).then(res => {
      firebase
        .database()
        .ref(`/tablet/${res[0].value}`)
        .push(dataObject)
        .then(
          () =>
            SqlService.update(
              "Config",
              ["value"],
              [day],
              "key = 'date_Sinc'",
              []
            ),
          SqlService.select("Config", "*")
            .then(x => {
              x.map(k => {
                switch (k["key"]) {
                  case "date_Sinc":
                    this.setState({ lastSinc: k.value, isLoading: false });
                    break;

                  case "last_idData_sinc":
                    SqlService.query(
                      "update config set value = (select id from data order by id desc limit 1) where key = 'last_idData_sinc'"
                    );
                    break;

                    case "obs_sinc":
                    SqlService.query(
                      "update config set value = (select id from observacao order by id desc limit 1) where key = 'obs_sinc'"
                    );
                    break;
                }
              });
            })
            .then(() => {
              Alert.alert(
                "Enviar dados",
                "Todos os dados foram enviados com Sucesso!"
              );
            })
        )
        .catch(e => Alert.alert("Erro", `Erro ao sincronizar os dados! ${e}`));
    });
    this.setState({ isLoading: false });
  }

  componentDidMount() {
    SqlService.select(
      "Config",
      "value",
      " key = 'date_Sinc'",
      "date_Sinc"
    ).then(x => this.setState({ dateSinc: x[0].value }));

    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this._handleConnectivityChange
    );

    NetInfo.isConnected.fetch().done(isConnected => {
      if (isConnected == true) {
        this.setState({
          connection_Status: { status: true, label: "  Conexão Online" }
        });
      } else {
        this.setState({
          connection_Status: {
            status: false,
            label: "  Sem conexão com a internet"
          }
        });
      }
    });

    if (!firebase.apps.length) {
      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyBlbrsrpFMTS37PqkPB_A50KWHaK8Lkuwo",
        authDomain: "testemetro-ca182.firebaseapp.com",
        databaseURL: "https://testemetro-ca182.firebaseio.com",
        projectId: "testemetro-ca182",
        storageBucket: "testemetro-ca182.appspot.com",
        messagingSenderId: "156524751497"
      };
      firebase.initializeApp(config);
    }
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this._handleConnectivityChange
    );
  }

  render() {
    let status = this.state.connection_Status.status;
    return (
      <View style={styles.container}>
        <Text
          style={[
            { marginTop: 20, fontSize: 25, paddingBottom: 10 },
            status ? { color: "green" } : { color: "red" }
          ]}
        >
          <Icon name={status ? "wifi" : "disconnect"} size={35} />
          {this.state.connection_Status.label}
        </Text>
        <Text style={{ marginTop: 10, fontSize: 23 }}>
          Última sincronização
        </Text>
        <Text style={{ marginTop: 10, fontSize: 20, paddingBottom: 40 }}>
          {this.state.dateSinc}
        </Text>
        {this.state.isLoading ? (
          <ActivityIndicator size="large" />
        ) : (
          <Button
            title="Sincronizar"
            disabled={!status}
            onPress={() => this.sinc()}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});
