import React from "react";
import { Text, View, ScrollView } from "react-native";
import ButtomComponent from "../components/ButtonComponent";
import SQlservice from "../../provider/SqlService";

export default class ContagemScreen extends React.Component {
  constructor(props) {
    super();
    this.state = {
      // session_id: -1,
      estacao: [
        {
          id: -1,
          estacao: "",
          linha: ""
        }
      ]
    };
  }

  async getData() {
    console.log('session ' + this.props.navigation.state.params.session_id)
    return SQlservice.select("estacoes", "*").then(res => res);
  }

  async componentDidMount() {
    let data = await this.getData();
    this.setState({ estacao: data });
  }

  render() {
    return (
      <View>
        <ScrollView>
          {this.state.estacao.map(e => {
            return (
              <ButtomComponent key={e.id} label={e.estacao} linha={e.linha} session_id={this.props.navigation.state.params.session_id} />
            );
          })}
        </ScrollView>
      </View>
    );
  }
}
