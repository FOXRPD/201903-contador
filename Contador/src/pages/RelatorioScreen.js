import React, { Component } from "react";
import { Text, View, StyleSheet, Button, Alert } from "react-native";
import SqlService from "../../provider/SqlService";
import moment from 'moment';

export default class RelatorioScreen extends Component {
  constructor() {
    super();
    this.state = {
      dados: [
        {
          hora: "",
          quantidade: ""
        }
      ]
    };
  }

  componentDidMount() {
    this.getData()
  }

  getData() {
    const hora = [
      "04",
      "05",
      "06",
      "07",
      "08",
      "09",
      "10",
      "11",
      "12",
      "13",
      "14",
      "15",
      "16",
      "17",
      "18",
      "19",
      "20",
      "21",
      "22",
      "23"
    ];

    let da = moment(new Date()).format("YYYY-MM-DD")
    let po = this.props.navigation.state.params.pontoSelecionado.id
    var promise = Promise.all(
      hora.map(h => {
        let s = SqlService.query(
          "select count(d.id) as quantidade from session s join point p on p.id = s.point_id join data d on d.session_id = s.id where s.user != 99999 and s.point_id == " + po + " and dateregistered like '" + da + " " +
            h +
            "%'"
        ).then(c => {
          console.log(c[0].quantidade)
          let quantidade = c[0].quantidade;
          return {
            hora: h + ':00',
            quantidade: quantidade
          };
        });

        return s;
      })
    );
      promise.then(p => this.setState({ dados: p }))
  }

  renderList() {
    const { dados } = this.state;

    let l = dados.map(d => {
      return(
      <View
        style={{ flexDirection: "row", borderTopWidth: 2, borderColor: 'gray' }}
        key={d.hora}
      >
        <Text style={styles.line}>{d.hora}</Text>
        <Text style={styles.line}>{d.quantidade}</Text>
      </View>
      )
    })
    //Header da planilha
    return (
      <View>
        <View style={{ flexDirection: "row" }}>
          <Text style={[styles.line, styles.header]}>Horário</Text>
          <Text style={[styles.line, styles.header]}>Quantidade</Text>
        </View>
        <View>{l}</View>
      </View>
    );
  }
  render() {
    return <View style={styles.container}>{this.renderList()}
    <Button title="Buscar" onPress={() => this.getData()} />
    </View>;
  }
}

const styles = StyleSheet.create({
  container: {
    marginLeft: 70,
    marginRight: 70,
    marginTop: 30,
    borderWidth: 2,
    borderColor: "gray",
    backgroundColor: "powderblue"
  },
  line: {
    width: "50%",
    fontSize: 25,
    borderRightWidth: 1,
    borderColor: "gray",
    textAlign: "center"
  },
  header: {
    fontWeight: 'bold',
    backgroundColor: 'steelblue',
    fontSize: 27,
    color: '#fff'
  }
});
