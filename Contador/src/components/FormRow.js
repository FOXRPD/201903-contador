import React from "React";
import { StyleSheet, View, Text } from "react-native";

const FormRow = props => {
  const { children, first, last } = props;
  return (
    <View style={[
        styles.container,
        first ? styles.first : null,
        last ? styles.last : null]}>
      {children}
    </View>
  );
};

export default FormRow;

const styles = StyleSheet.create({
  container: {
    padding: 0,
    backgroundColor: "white",
    marginTop: 5,
    marginBottom: 5,
    // elevation: 1
    borderColor: '#bebebe',
  },
  first: {
    marginTop: 10
  },
  last: {
    paddingBottom: 10
  }
});
