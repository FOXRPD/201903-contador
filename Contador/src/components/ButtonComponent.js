import React from "react";
import {
  Text,
  View,
  Image,
  TouchableHighlight,
  StyleSheet
} from "react-native";
import SqlService from "../../provider/SqlService";
import moment from "moment";

const ButtonComponent = props => {
  const { label, linha, session_id } = props;

  let color = "#772780";

  switch (linha) {
    case "Amarela":
      color = "#ffff00";
      break;

    case "Verde":
      color = "#008f00";
      break;

    case "Azul":
      color = "#0010dd";
      break;
  }

  return (
    <View style={styles.container}>
      <TouchableHighlight
        style={[styles.btn, color ? { backgroundColor: color } : null]}
        underlayColor="#ff8000"
        onPress={() =>
          SqlService.insert(
            "data",
            ["dateRegistered", "session_id", "entrada", "isDeleted"],
            [moment(new Date()).format("YYYY-MM-DD HH:mm:ss"), session_id, label, 0]
          )
        }
      >
        <Text
          style={[styles.text, color == "#ffff00" ? { color: "#555" } : null]}
        >
          {label}
        </Text>
      </TouchableHighlight>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 150
  },
  btn: {
    width: 280,
    height: 50,
    borderWidth: 1,
    borderRadius: 20,
    borderColor: "#292929",
    backgroundColor: "#772792",
    margin: 8
  },
  text: {
    textAlign: "center",
    marginTop: 7,
    fontSize: 25,
    color: "#fff",
    fontWeight: "bold"
  }
});

export default ButtonComponent;
