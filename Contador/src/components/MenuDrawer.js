import React, { Component } from "react";
import {
  Text,
  View,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Alert
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";

const WIDTH = Dimensions.get("window").width;
const HEIGHT = Dimensions.get("window").height;

export default class MenuDrawer extends Component {
  navlink(nav, text, icon) {
    return (
      <TouchableOpacity
        style={{ height: 60 }}
        onPress={() => {
          console.log(nav);
          if (nav == "Login") {
            Alert.alert(
              "Finalizar",
              "Deseja finalizar a Sessão?",
              [
                {
                  text: "Cancelar"
                },
                {
                  text: "Finalizar",
                  onPress: () => this.props.navigation.navigate(nav)
                }
              ],
              { cancelable: false }
            );
          } else {
            console.log(nav);
            this.props.navigation.navigate(nav);
          }
        }}
      >
        <View style={styles.imageLink}>
          <Icon name={icon} size={35} />
          <Text style={styles.link}>{text}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topLinks}>
          <View style={{ flexDirection: "row" }}>
            <Text
              style={[styles.textTop, { fontWeight: "bold", fontSize: 26 }]}
            >
              Usuário:
            </Text>
            <Text style={styles.textTop}>
              {this.props.navigation.state.params.usuario}
            </Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text
              style={[styles.textTop, { fontWeight: "bold", fontSize: 26 }]}
            >
              Ponto:
            </Text>
            <Text style={[styles.textTop, {width: 380}]}>
              {this.props.navigation.state.params.pontoSelecionado.label}
            </Text>
          </View>
        </View>
        <View style={styles.bottomLinks}>
          {this.navlink("Contagem", "Pesquisa", "user")}
          {this.navlink("Observacao", "Observação", "edit")}
          {this.navlink("Coordenador", "Relatório", "filetext1")}
          {this.navlink("Login", "Finalizar Sessão", "close")}
        </View>
        <Text>Versão 1.0</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "lightgray"
  },
  topLinks: {
    height: 160,
    backgroundColor: "gray"
  },
  textTop: {
    paddingLeft: 20,
    paddingTop: 20,
    color: "white",
    fontSize: 25
  },
  bottomLinks: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 10,
    paddingBottom: 10
  },
  imageLink: {
    flexDirection: "row",
    paddingLeft: 14,
    marginBottom: 100,
    marginTop: 30,
    marginRight: 100,
    alignItems: "center"
  },
  link: {
    fontSize: 22,
    paddingLeft: 15,
    textAlign: "left"
    // margin: 20,
  }
});
