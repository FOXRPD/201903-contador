const SQLite = require("react-native-sqlite-storage"); // plugin
const NativeEventEmitter = require("EventEmitter"); // super worked
import RNFS from "react-native-fs";
import { Alert } from "react-native";

const dbFolder = RNFS.ExternalDirectoryPath + "/db.db3";

class SqlService extends NativeEventEmitter {
  constructor(props) {
    super(props);
  }

  async execute(sql, value, type) {
    type = type || "array";

    let db = SQLite.openDatabase(dbFolder, "1.0", "React Database", 200000);

    const tx = await new Promise((resolve, reject) => {
      db.transaction(resolve, code => {
        Alert.alert("Erro no SQL", code.message);
      });
    });

    return await new Promise((resolve, reject) => {
      switch (type) {
        case "array":
          tx.executeSql(sql, value, (tx, res) => {
            let list = [];
            res = res;
            for (var i = 0; i < res.rows.length; i++)
              list.push(res.rows.item(i));
            resolve(list);
          });
          break;
        case "object":
          tx.executeSql(sql, value, (tx, res) => {
            res = res;
            resolve(res.rows.item(0));
          });
          break;
        default:
          tx.executeSql(sql, value, (tx, res) => {
            let list = [];
            res = res;
            for (var i = 0; i < res.rows.length; i++)
              list.push(res.rows.item(i));
            resolve(list);
          });
          break;
      }
    });
  }

  async select(table, field = "*", where = "", values = null, order = null) {
    var list = [];

    if (where && values) {
      var sql = "SELECT " + field + " FROM " + table + " WHERE " + where;
    } else {
      var sql = "SELECT " + field + " FROM " + table;
    }
    if (order) sql += " ORDER BY " + order;

    return this.execute(sql, values || [], null).then(function(res) {
      return res;
    });
  }

  async insert(table, row, values) {
    var sql = "INSERT INTO " + table + " (";
    for (var i = 0; i < row.length; i++) sql += row[i] + ",";
    sql = sql.slice(0, sql.length - 1);
    sql += ") VALUES ";

    if (typeof values[0] == "object") {
      for (var i = 0; i < values.length; i++) {
        sql += "(";
        for (var j = 0; j < values[i].length; j++)
          sql +=
            j != values[i].length - 1
              ? "'" + values[i][j] + "',"
              : "'" + values[i][j] + "'),";
      }
      sql = sql.slice(0, sql.length - 1);
      values = [];
    } else {
      sql += "(";
      for (var i = 0; i < values.length; i++) sql += "?,";
      sql = sql.slice(0, sql.length - 1);
      sql += ")";
    }

    return await this.execute(sql, values || [], "popup").then(function(res) {
      return res;
    });
  }

  delete(table, where = null, values = null) {
    if (where && values) var sql = "DELETE FROM " + table + " WHERE " + where;
    else var sql = "DELETE FROM " + table;

    return this.execute(sql, values || [], "popup").then(function(res) {
      return res;
    });
  }

  update(table: string, row, values, where: string, wValues: Array<any>) {
    var sql = "UPDATE " + table + " SET ";
    for (var i = 0; i < values.length; i++) sql += row[i] + "=?,";
    sql = sql.slice(0, sql.length - 1);
    if (where && wValues) {
      sql += " WHERE " + where;

      for (var i = 0; i < wValues.length; i++) values.push(wValues[i]);

      console.log("where " + sql);
    }

    console.log(sql);
    return this.execute(sql, values || [], "popup").then(function(res) {
      return res;
    });
  }

  query(sql: string, values: Array<any>): Promise<any> {
    return this.execute(sql, values, "popup").then(function(res) {
      return res;
    });
  }

  createDatabase() {
    return RNFS.exists(dbFolder).then(exists => {
      if (exists) {
        console.log("DB encontrado");
        return true;
      } else {
        console.log("Criando DB...");

        console.log(dbFolder);

        this.query("CREATE TABLE User (id INTEGER NOT NULL)");

        this.query(
          "CREATE TABLE Point (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, desc VARCHAR(255) NOT NULL)"
        );
        this.query(
          "CREATE TABLE Session (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, datetime DATETIME NOT NULL,user INTEGER NOT NULL,point_id INTEGER NOT NULL)"
        );
        this.query(
          "CREATE TABLE data (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, dateRegistered DATETIME NOT NULL, session_id INTEGER NOT NULL, entrada VARCHAR(255) NOT NULL, isDeleted INTEGER NOT NULL)"
        );
        this.query(
          "CREATE TABLE Config (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,`key` VARCHAR(30) NOT NULL,value VARCHAR(255) NOT NULL)"
        );
        this.query(
          "CREATE TABLE Estacoes (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, estacao VARCHAR(255) NOT NULL, linha VARCHAR(255) NOT NULL)"
        );
        this.query(
          "CREATE TABLE Observacao (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, datetime DATETIME NOT NULL, session_id INTEGER NOT NULL, texto VARCHAR(255) NOT NULL)"
        );

        let a = [
          "Capão Redondo_Lilas",
          "Campo Limpo_Lilas",
          "Vila das Belezas_Lilas",
          "Giovanni Gronchi_Lilas",
          "Santo Amaro_Verde",
          "Largo Treze_Lilas",
          "Adolfo Pinheiro_Lilas",
          "Alto da Boa Vista_Lilas",
          "Borba Gato_Lilas",
          "Brookiln_Lilas",
          "Campo Belo_Amarela",
          "Eucaliptos_Lilas",
          "Moema_Lilas",
          "AACD-Servidor_Lilas",
          "Hospital São Paulo_Lilas",
          "Santa Cruz_Azul",
          "Chácara Klabin_Verde"
        ];

        a.map(e =>
          this.query(
            "INSERT INTO Estacoes (estacao, linha) VALUES ('" +
              e.split("_")[0] +
              "','" +
              e.split("_")[1] +
              "')"
          )
        );

        let u = [
          10018,
          10087,
          10118,
          10482,
          10548,
          10607,
          10709,
          10755,
          10826,
          10852,
          10855,
          10874,
          10963,
          11170,
          11189,
          11265,
          11361,
          11449,
          11481,
          11550,
          11621,
          11633,
          11656,
          11747,
          11797,
          11814,
          11942,
          11943,
          12073,
          12081,
          12149,
          12280,
          12282,
          12314,
          12337,
          12503,
          12817,
          12857,
          12872,
          12879,
          13019,
          13109,
          13126,
          13271,
          13450,
          13499,
          13563,
          13628,
          13765,
          13795,
          13834,
          13873,
          13910,
          13926,
          13979,
          13995,
          14034,
          14077,
          14197,
          14323,
          14380,
          14417,
          14434,
          14538,
          14575,
          14644,
          14709,
          14753,
          14835,
          14850,
          14893,
          15024,
          15033,
          15151,
          15164,
          15293,
          15295,
          15363,
          15399,
          15500,
          15504,
          15589,
          15623,
          15628,
          15684,
          15718,
          15755,
          15865,
          15937,
          15968,
          15989,
          16018,
          16025,
          16033,
          16076,
          16117,
          16246,
          16305,
          16540,
          16734,
          16791,
          16829,
          16908,
          16947,
          17022,
          17042,
          17086,
          17123,
          17283,
          17285,
          17321,
          17338,
          17383,
          17415,
          17430,
          17550,
          17567,
          17588,
          17701,
          17784,
          17829,
          17978,
          18073,
          18089,
          18158,
          18160,
          18169,
          18219,
          18463,
          18480,
          18548,
          18583,
          18587,
          18619,
          18718,
          18748,
          18803,
          18867,
          18869,
          18880,
          18889,
          18890,
          19005,
          19042,
          19083,
          19295,
          19373,
          19392,
          19394,
          19415,
          19430,
          19433,
          19491,
          19522,
          19548,
          19554,
          19622,
          19650,
          19666,
          19689,
          19728,
          20010,
          20011,
          20062,
          20090,
          20129,
          20156,
          20161,
          20186,
          20229,
          20238,
          20242,
          20271,
          20361,
          20396,
          20408,
          20417,
          20493,
          20534,
          20539,
          20693,
          20806,
          20838,
          20886,
          21064,
          21067,
          21118,
          21169,
          21180,
          21229,
          21372,
          21377,
          21409,
          21440,
          21593,
          21603,
          21610,
          21708,
          21847,
          21871,
          21884,
          21902,
          21917,
          21923,
          21930,
          21937,
          22006,
          22017,
          22160,
          22284,
          22381,
          22423,
          22436,
          22490,
          22508,
          22571,
          22741,
          22818,
          22941,
          22962,
          22970,
          22989,
          23002,
          23102,
          23124,
          23252,
          23360,
          23486,
          23526,
          23659,
          23691,
          23809,
          23964,
          23993,
          24044,
          24120,
          24221,
          24231,
          24246,
          24258,
          24275,
          24411,
          24445,
          24596,
          24707,
          24727,
          24892,
          24901,
          24930,
          24956,
          24968,
          24985,
          25061,
          25066,
          25083,
          25125,
          25189,
          25236,
          25295,
          25319,
          25338,
          25341,
          25394,
          25423,
          25549,
          25581,
          25590,
          25593,
          25613,
          25780,
          25959,
          26096,
          26112,
          26259,
          26270,
          26278,
          26353,
          26362,
          26471,
          26477,
          26522,
          26690,
          26781,
          26958,
          26995,
          27006,
          27087,
          27127,
          27133,
          27243,
          27346,
          27368,
          27422,
          27457,
          27463,
          27483,
          27539,
          27570,
          27573,
          27616,
          27621,
          27758,
          27866,
          27965,
          28007,
          28013,
          28032,
          28157,
          28228,
          28242,
          28365,
          28379,
          28400,
          28404,
          28430,
          28442,
          28546,
          28547,
          28565,
          28677,
          28680,
          28799,
          28829,
          28847,
          28898,
          28919,
          28935,
          28942,
          28946,
          28958,
          29052,
          29070,
          29139,
          29173,
          29176,
          29221,
          29247,
          29248,
          29459,
          29748,
          29798,
          29920,
          29936,
          29938,
          99148,
          99169,
          99179,
          99244,
          99294,
          99522,
          99652,
          99714,
          99780,
          99940,
          99966,
          99999,
          1235468,
          1546325,
          1588963,
          1864523,
          2351865,
          4853215,
          6543215,
          8521664,
          8564231,
          9832156
        ];

        let query = "";
        for (var i = 0; i < u.length; i++) {
          if (i != 0) {
            query += ",(" + u[i] + ")";
          } else {
            query = "(" + u[0] + ")";
          }
        }

        this.query("INSERT INTO User (id) VALUES " + query);

        let p = [
          "Capão Redondo - Plataforma de Embarque",
          "Campo Limpo - Bloqueios",
          "Vila Das Belezas - Bloqueios",
          "Giovanni Gronchi - Bloqueios",
          "Santo Amaro - Bloqueios Guido Caloi",
          "Santo Amaro - Bloqueios CPTM",
          "Largo Do Treze - Bloqueios Terminal Santo Amaro",
          "Largo Do Treze - Bloqueios Barão do Rio Branco",
          "Adolfo Pinheiro - Bloqueios",
          "Alto da Boa Vista - Bloqueios",
          "Borba Gato - Bloqueios",
          "Brooklin - Bloqueios",
          "Eucaliptos - Mezanino",
          "Moema - Bloqueios",
          "AACD-Servidor - Bloqueios",
          "Hospital São Paulo - Bloqueios",
          "Santa Cruz - Terceiro pavimento abaixo dos Bloqueios, entre as escadas",
          "Chácara Klabin - Plataforma de Embarque"
        ];

        for (var i = 0; i < p.length; i++) {
          if (i != 0) {
            query += ",('" + p[i] + "')";
          } else {
            query = "('" + p[0] + "')";
          }
        }

        this.query("INSERT INTO Point (desc) VALUES " + query);

        console.log("Criação Finalizada com Sucesso!");
        exist = false;
      }
    });
  }
}
module.exports = new SqlService();
